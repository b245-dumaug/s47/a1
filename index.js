// console.log('JS DOM - Manipulation');

// NOTE: Document object Model (DOM)
// allows us to access or modify the properties of an HTML element in a webpage
// it is stantard on how to get , change , add or delete HTML elements
//  we will be focusing only with DOM in terms of manging forms.

// For selection HTML elements we will be be using docuemnt.querySelector / getElementById
// Syntax : document.querySelector("html element")

//CSS seletors
// class selector (.);
// id selector  (#);
// tag selector (html tags);
// universal (*);
// attribute selector ([attribute]);

let universalSelector = document.querySelector('*');

let singleUniversalSelector = document.querySelector('*');

console.log(universalSelector);

let classSelector = document.querySelectorAll('.full-name');

console.log(classSelector);

let singleClassSelector = document.querySelector('.full-name');

console.log(singleClassSelector);

let targetSelector = document.querySelectorAll('input');

console.log(targetSelector);

let spanSelector = document.querySelector('span[id]');

console.log(spanSelector);

//getElement
let element = document.getElementById('fullName');

// element = document.classElementByClassName('full-name');

// console.log(element);

//[Section] Event listeners
// whenever a user interacts with a webpage, this action is considered as an avent
//working with events is large part of creating interactivity in a web page.
//  specific funtion that will be triggered if the event happend

//The function use is "addEventListener", it takes two arguments
// first argument a string identifying the event.
// second argument, function that the listener wil; trigger once the spsecifie event occur

let txtFirstName = document.querySelector('#txt-first-name');

// add event listener
txtFirstName.addEventListener('keyup', () => {
  console.log(txtFirstName.value);
  spanSelector.innerHTML = `${txtFirstName.value}`;
});

let txtLastName = document.querySelector('#txt-last-name');

txtLastName.addEventListener('keyup', () => {
  spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
});

let textColor = document.querySelector('#text-color');
let fullName = document.querySelector('#fullName');

textColor.addEventListener('change', () => {
  fullName.style.color = textColor.value;
});
